// SPDX-License-Identifier: GPL-2.0

//! Nova GPU Driver

mod bios;
mod driver;
mod file;
mod fwsec;
mod gem;
mod gsp;
mod gpu;

use crate::driver::NovaDriver;

kernel::module_pci_driver! {
    type: NovaDriver,
    name: "Nova",
    author: "Danilo Krummrich",
    description: "Nova GPU driver",
    license: "GPL v2",
}
