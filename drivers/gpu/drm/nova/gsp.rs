use kernel::{
    alloc::flags::*,
    error::{Result, code::*},
    page::*,
    device,
    dma::CoherentAllocation,
    scatterlist::{*, DmaDataDirection::*},
    elf::Elf,
    prelude::{KVec, VVec},
    pci,
};
use core::alloc::Layout;

const GSP_PAGE_SHIFT: usize = 12;
const GSP_PAGE_SIZE: usize = 1 << GSP_PAGE_SHIFT;

/// A radix3 table that maps our GSP firmware.
///
/// The GSP uses a three-level page table, called radix3, to map the firmware.
/// Each 64-bit "pointer" in the table is either the bus address of an entry in
/// the next table (for levels 0 and 1) or the bus address of the next page in
/// the GSP firmware image itself.
///
/// Level 0 contains a single entry in one page that points to the first page
/// of level 1.
///
/// Level 1, since it's also only one page in size, contains up to 512 entries,
/// one for each page in Level 2.
///
/// Level 2 can be up to 512 pages in size, and each of those entries points to
/// the next page of the firmware image.  Since there can be up to 512*512
/// pages, that limits the size of the firmware to 512*512*GSP_PAGE_SIZE = 1GB.
///
/// Internally, the GSP has its window into system memory, but the base
/// physical address of the aperture is not 0.  In fact, it varies depending on
/// the GPU architecture.  Since the GPU is a PCI device, this window is
/// accessed via DMA and is therefore bound by IOMMU translation.  The end
/// result is that GSP-RM must translate the bus addresses in the table to GSP
/// physical addresses.  All this should happen transparently.
///
/// Returns a GspRadix3 object on success.
#[allow(dead_code)]
struct GspRadix3(KVec<CoherentAllocation<u64>>);

impl GspRadix3 {
    /// Allocate a radix3 table from a scatter-gather list
    pub (crate) fn new(mut size: usize,
                       sg_init: &mut SGTableInit,
                       dev: &device::Device) -> Result<GspRadix3>
    {
        let mut mem: KVec<CoherentAllocation<u64>> = KVec::new();
        let mut addr: u64 = 0;

        // Build the 3-level page table
        for level in (0..3).rev() {
            let mut index = 0;
            let tbl_size: usize =
                Layout::from_size_align((size as usize / GSP_PAGE_SIZE)
                                        * core::mem::size_of::<u64>(),
                                        GSP_PAGE_SIZE)?.pad_to_align().size();
            if tbl_size == 0 {
                return Err(EINVAL);
            }
            let mut ca: CoherentAllocation<u64> =
                CoherentAllocation::alloc_coherent(dev, tbl_size, GFP_KERNEL)?;

            if level == 2 {
                for sg in sg_init.iter() {
                    for j in 0..(sg.dma_len() / GSP_PAGE_SIZE) {
                        let entry: u64 = sg.dma_address() + (GSP_PAGE_SIZE as u64 * j as u64);
                        ca.write(index, &entry)?;
                        index += 1;
                    }
                }
            } else {
                for j in 0..size / GSP_PAGE_SIZE {
                    let entry: u64 = addr + GSP_PAGE_SIZE as u64 * j as u64;
                    ca.write(j, &entry)?;
                }
            }

            addr = ca.dma_handle();
            mem.push(ca, GFP_KERNEL)?;
            size = tbl_size;
        }
        Ok(Self {
            0: mem,
        })
    }
}

/// Structure containing resources for the GSP firmware.
#[allow(dead_code)]
pub(crate) struct Gsp {
    radix3: GspRadix3,
    sgt: SGTable,
}

impl Gsp {
    /// Create an sgtable from the GSP firmware
    fn load_fw_sgt(dev: &device::Device,
                   sgt: &mut SGTable,
                   fw: &[u8]) -> Result<SGTableInit>
    {
        let pages = Layout::from_size_align(fw.len(),
                                             PAGE_SIZE)?.pad_to_align().size() >> PAGE_SHIFT;
        let buf: VVec<PageSlice> = fw.try_into()?;
        let mut s_init = sgt.alloc_table(pages as u32, GFP_KERNEL)?;

        for (i, sg) in s_init.iter().enumerate() {
            sg.set_page(Page::page_slice_to_page(&buf[i])?, PAGE_SIZE as u32, 0);
        }
        s_init.dma_map(sgt, dev, DMA_TO_DEVICE)?;
        Ok(s_init)
    }

    /// Allocate the Gsp object containing resources to make use of the GSP firmware.
    pub(crate) fn new(gspfw: &VVec<u8>, dev: &pci::Device) -> Result<Gsp>
    {
        let elf = Elf::from_bytes(gspfw.as_slice())?;
        let mut sgt = SGTable::uninit()?;

        for i in 0..elf.section_header_count() {
            if elf.section_name(i)?.to_str() == core::prelude::v1::Ok(".fwimage") {
                let data = elf.section_bytes(i)?;
                let mut s_init = Gsp::load_fw_sgt(dev.as_ref(), &mut sgt, data)?;
                let radix3 = GspRadix3::new(data.len(), &mut s_init, dev.as_ref())?;

                return Ok(Self{radix3,
                               sgt,
                })
            }
        }

        return Err(EINVAL)
    }
}
