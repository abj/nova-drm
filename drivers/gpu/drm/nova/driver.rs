// SPDX-License-Identifier: GPL-2.0

use kernel::{
    bindings, c_str, drm,
    drm::{drv, ioctl},
    pci,
    pci::define_pci_id_table,
    prelude::*,
    sync::Arc,
};

use crate::{file::File, gpu::Gpu};

pub(crate) struct NovaDriver;

/// Convienence type alias for the DRM device type for this driver
pub(crate) type NovaDevice = drm::device::Device<NovaDriver>;

#[allow(dead_code)]
pub(crate) struct NovaData {
    pub(crate) gpu: Arc<Gpu>,
    pub(crate) pdev: pci::Device,
}

const BAR0_SIZE: usize = 0x1000000;
pub(crate) type Bar0 = pci::Bar<BAR0_SIZE>;

const INFO: drm::drv::DriverInfo = drm::drv::DriverInfo {
    major: 0,
    minor: 0,
    patchlevel: 0,
    name: c_str!("nova"),
    desc: c_str!("Nvidia Graphics"),
    date: c_str!("20240227"),
};

impl pci::Driver for NovaDriver {
    type Data = Arc<NovaData>;

    define_pci_id_table! {
        (),
        [ (pci::DeviceId::new(bindings::PCI_VENDOR_ID_NVIDIA, bindings::PCI_ANY_ID as u32), None) ]
    }

    fn probe(pdev: &mut pci::Device, _id_info: Option<&Self::IdInfo>) -> Result<Self::Data> {
        dev_dbg!(pdev.as_ref(), "Probe Nova GPU driver.\n");

        pdev.enable_device_mem()?;
        pdev.set_master();

        let bar = pdev.iomap_region_sized::<BAR0_SIZE>(0, c_str!("nova"))?;

        let gpu = Gpu::new(pdev, bar)?;
        gpu.init()?;

        let data = Arc::new(
            NovaData {
                gpu,
                pdev: pdev.clone(),
            },
            GFP_KERNEL,
        )?;

        let drm = drm::device::Device::<NovaDriver>::new(pdev.as_ref(), data.clone())?;
        drm::drv::Registration::new_foreign_owned(drm, 0)?;

        Ok(data)
    }

    fn remove(data: &Self::Data) {
        dev_dbg!(data.pdev.as_ref(), "Remove Nova GPU driver.\n");
    }
}

#[vtable]
impl drm::drv::Driver for NovaDriver {
    type Data = Arc<NovaData>;
    type File = File;
    type Object = crate::gem::Object;

    const INFO: drm::drv::DriverInfo = INFO;
    const FEATURES: u32 = drv::FEAT_GEM;

    kernel::declare_drm_ioctls! {
        (NOVA_GETPARAM, drm_nova_getparam, ioctl::RENDER_ALLOW, File::get_param),
        (NOVA_GEM_CREATE, drm_nova_gem_create, ioctl::AUTH | ioctl::RENDER_ALLOW, File::gem_create),
        (NOVA_GEM_INFO, drm_nova_gem_info, ioctl::AUTH | ioctl::RENDER_ALLOW, File::gem_info),
    }
}
