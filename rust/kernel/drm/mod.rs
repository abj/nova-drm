// SPDX-License-Identifier: GPL-2.0 OR MIT

//! DRM subsystem abstractions.

pub mod device;
pub mod drv;
pub mod file;
pub mod gem;
pub mod ioctl;
